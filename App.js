import React,{useState} from 'react';
import MapView, { Marker, Circle,Callout}  from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions, Plateform } from 'react-native';


export default function App() {

   const [pin, setPin] = useState({
    latitude: 45.73196379224408,
    longitude: 4.826410705854247,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
  })

  return (
    <View style={styles.container}>
      <MapView style={styles.map} initialRegion ={{
       latitude: 45.73196379224408,
       longitude: 4.826410705854247,
       latitudeDelta: 0.0922,
       longitudeDelta: 0.0421,
      }}>

      <Marker coordinate={pin} pinColor={'red'}>
        <Callout>
          <Text> OK GOOGLE</Text>
        </Callout>
      </Marker>
      <Circle
        center={{
          latitude: 45.73196379224408,
          longitude: 4.826410705854247
        }}
        radius ={1000}
      />


      </MapView>
      <Text>Test</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
